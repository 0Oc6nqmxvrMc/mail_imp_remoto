****************************************************************************** 
**		PATH ENVELOPADORA REMOTA - SR JULIO									**
******************************************************************************
#DEFINE DIRSERVIMP		"X:\SERVIMP\" 
#DEFINE DIRDATA			"X:\SERVIMP\SRAINHA_IMP\DATA\"
#DEFINE DIRDIARIO		"X:\SERVIMP\SRAINHA_IMP\DIARIO\"
#DEFINE DIRRELAT		"X:\SERVIMP\SRAINHA_IMP\DIARIO\RELATORIOS\"
#DEFINE DIRSPROGS		"X:\SERVIMP\SRAINHA_IMP\PROGS\" 
#DEFINE DIRDOCS  		"X:\SERVIMP\SRAINHA_IMP\DOCS\"
#DEFINE DIRIMPRESSAO	"X:\"
#DEFINE	DIRARQS			"X:\SERVIMP\SRAINHA_IMP\ARQ_DIA\"
#DEFINE	DIRARQSPED		"X:\SERVIMP\SRAINHA_IMP\ARQ_DIA\PEDIDOS\"
#DEFINE	DIRARQSCART		"X:\SERVIMP\SRAINHA_IMP\ARQ_DIA\CARTAS\"
#DEFINE DIRERROS		"X:\SERVIMP\SRAINHA_IMP\ERR\"

#DEFINE TTAB			"\\192.168.172.11\Impressao-vinde\TAB_REG"
#DEFINE TTABE13			"X:\SERVIMP\SRAINHA_IMP\DATA\TAB_REGE13"
#DEFINE TCART			"X:\SERVIMP\SRAINHA_IMP\DATA\TAB_CART"
#DEFINE TPED			"X:\SERVIMP\SRAINHA_IMP\DATA\TAB_PED"

#DEFINE DIREXE			"X:\SERVIMP\SRAINHA_IMP\PROGS\MAIL_IMP_REMOTO.EXE" 