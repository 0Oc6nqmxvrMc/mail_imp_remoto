* Save this File to SetMemory.PRG
* Add 2 (two) lines in you main program:
* 1.  Set Procedure to SetMemory.PRG additive &&(if you have more than one Procedure File)
* 2.  =SetMemory()
*

LOCAL lnAvailableMem, lpMemoryStatus, lnPct

* Running on the desktop
* Agit72 : agit72@plasa.com, admin@msp-works.com
          
DECLARE GlobalMemoryStatus IN Win32API STRING @lpMemoryStatus
lpMemoryStatus = REPLICATE(CHR(0), 32)
GlobalMemoryStatus(@lpMemoryStatus)

lnAvailableMem = CharToBin(SUBSTR(lpMemoryStatus, 13, 4))

*** EGL: - Added the fine tuning option
lnPct = 1
IF TYPE("goApp.nSetMemoryPct") == "N"
    lnPct = goApp.nSetMemoryPct
ENDIF
lnAvailableMem = (lnAvailableMem * lnPct)

SYS(3050, 1, lnAvailableMem)
SYS(3050, 2, (lnAvailableMem/2) )

RETURN lnAvailableMem


***************************
FUNCTION CharToBin (tcWord)
***************************
    LOCAL lnChar, lnWord
    lnWord = 0
    FOR lnChar = 1 TO LEN(tcWord)
        lnWord = lnWord + (ASC(SUBSTR(tcWord, lnChar, 1)) * (2 ^ (8 * (lnChar - 1))))
    ENDFOR

    RETURN lnWord
ENDFUNC