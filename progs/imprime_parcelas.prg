*!*
*!* Algorítimo adaptado para a mescla de tres impressões por folha
*!*
priarea=ALIAS()
GO TOP
SELECT * FROM ALIAS() INTO CURSOR segundo READWRITE
SELECT * FROM ALIAS() INTO CURSOR terceiro READWRITE
SELECT * FROM ALIAS() where RECNO() = 0 INTO CURSOR carnetemp READWRITE
SELECT (priarea)
rc=RECCOUNT()
nRest = MOD(rc,3)
IF nRest !=0
	rc = rc + nRest
ENDIF
nskip = (rc/3)
SKIP nskip IN segundo
SKIP (nskip*2) IN terceiro
DO WHILE RECNO() < (nskip + 1)
	SCATTER MEMVAR
	INSERT INTO carnetemp FROM MEMVAR 
	SELECT segundo
	SCATTER MEMVAR
	INSERT INTO carnetemp FROM MEMVAR 
	SELECT terceiro
	SCATTER MEMVAR
	INSERT INTO carnetemp FROM MEMVAR 
	SELECT (priarea)
	IF !EOF([segundo])
		SKIP IN segundo
	ENDIF
	IF !EOF([terceiro])
		SKIP IN terceiro
	ENDIF
	SKIP
ENDDO
SELECT (priarea)
USE
SELECT carnetemp
SELECT * FROM carnetemp INTO CURSOR xx readwrite
*