*********************************************************************
* Rotina de c�lculo dos d�gitos verificadores da cobran�a s/registro 
* Banco Brasileiro de Descontos (Bradesco)
*********************************************************************
*
*********************************************************************
* C�lculo do d�gito verificador do nosso_num do Santander
*********************************************************************
*
v_resto       = 0
v_string      = PADL(ALLTRIM(RECIBO),12,[0])
v_DVNosso_Num = ""
iposicao      = LEN(v_string)
multiplicador = 2
v_Total       = 0
Do While .T.
	variavel  = "x" + AllTrim(Str(iposicao))
	&variavel = (val(subs(v_string,iposicao,1))) * multiplicador
	v_Total   = v_Total + &variavel
	multiplicador = multiplicador + 1
	If multiplicador > 9
		multiplicador = 2
	EndIf
	iposicao = iposicao - 1
	If iposicao <=0
		Exit
	EndIf
EndDo

v_resto = Mod( v_Total , 11 )

Do Case

	Case v_resto = 10
		v_DVNosso_Num =  "1"
		
	Case v_resto = 1 OR v_resto = 0
		v_DVNosso_Num =  "0"
		
	Otherwise
		v_DVNosso_Num = ALLTRIM(STR((11 - v_resto)))

EndCase
*
repl dv_ns with v_DVNosso_Num, nossonum with PADL(RECIBO + v_DVNosso_Num,FSIZE('nossonum'),[0]) ,;
	 nosso_num with PADL(RECIBO,FSIZE('nosso_num'),[0]), linha1 WITH STUFF(linha1,40,1,dv_ns)
*
*********************************************************************
* C�lculo do d�gito verificador dos dados do c�digo de barras
*********************************************************************
*
v_string      = alltrim(linha1)
multiplicador = 2
iposicao      = 44
TOTAL         = 0
DO while .t.
	IF iposicao <> 5
		variavel = "x" + alltr(str(iposicao))
		&variavel = (val(subs(v_string,iposicao,1))) * multiplicador
		TOTAL = total + &variavel
		multiplicador = multiplicador + 1
		IF multiplicador > 9
			multiplicador = 2
		ENDIF
		iposicao = iposicao - 1
		IF iposicao <=0
			EXIT
		ENDIF
	ELSE
		iposicao = iposicao - 1
	ENDIF
ENDDO

vsubs    =  11 - mod( total , 11 )

if vsubs = 0 .or. vsubs > 9
	vsubs = 1
endif

v_string = stuff(v_string,5,1,alltr(str(vsubs)))
REPL linha1 with v_string, dv with alltr(str(vsubs))
*
*********************************************************************
* C�lculo do d�gito verificador do campo (1) da linha digit�vel
*********************************************************************
*
multiplicador = 2
acumulador1   = 0
*
for d=len(campo1) to 1 Step -1
	v_dv1 = val(subs(campo1,d,1)) * multiplicador
	if v_dv1 > 9
		v_dv1 = val(subs(ltrim(str(v_dv1)),1,1)) + val(subs(ltrim(str(v_dv1)),2,1))
	endif
	acumulador1 = acumulador1 + v_dv1
	multiplicador = iif(multiplicador=1,2,1)
next
v_divisao1 = mod(acumulador1 , 10)
if v_divisao1 = 0
	v_dv1 = 0
else
	v_dv1 = 10 - v_divisao1
endif
*
repl dv1 with alltr(str(v_dv1))
*
*********************************************************************
* C�lculo do d�gito verificador do campo (2) da linha digit�vel
*********************************************************************
* 
multiplicador = 2
acumulador2   = 0
*
for d=len(campo2) to 1 Step -1
	v_dv2 = val(subs(campo2,d,1)) * multiplicador
	if v_dv2 > 9
		v_dv2 = val(subs(ltrim(str(v_dv2)),1,1)) + val( subs(ltrim(str(v_dv2)),2,1))
	endif
	acumulador2 = acumulador2 + v_dv2
	multiplicador = iif(multiplicador=1,2,1)
next
v_divisao2 = mod(acumulador2 , 10)
if v_divisao2 = 0 
	v_dv2 = 0
else
	v_dv2 = 10 - v_divisao2
endif
*
repl dv2 with alltr(str(v_dv2))
*
*********************************************************************
* C�lculo do d�gito verificador do campo (3) da linha digit�vel
*********************************************************************
*
*
multiplicador = 2
acumulador3   = 0
campo3        = STUFF(campo3,6,1,dv_ns)
*
for d=len(campo3) to 1 Step -1
	v_dv3 = val(subs(campo3,d,1)) * multiplicador
	if v_dv3 > 9
		v_dv3 = val(subs(ltrim(str(v_dv3)),1,1)) + val( subs(ltrim(str(v_dv3)),2,1))
	endif
	acumulador3 = acumulador3 + v_dv3
	multiplicador = iif(multiplicador=1,2,1)
next
v_divisao3 = mod(acumulador3 , 10)
if v_divisao3 = 0
	v_dv3 = 0
else
	v_dv3 = 10 - v_divisao3
endif
*
repl dv3 with alltr(str(v_dv3))
*
