PARAMETERS pbanco,pcampo,psigla_assoc
LOCAL lbanco,lcampo,lTexto,lsigla_assoc
lbanco = pbanco
lcampo = pcampo
IF PARAMETERS() = 3
	lsigla_assoc = ALLTRIM(psigla_assoc)
ENDIF
DO CASE 

	CASE LBANCO = "001"
		IF lcampo = "DS_BANCO"
			lTexto = "BANCO DO BRASIL"
		ENDIF

		IF lcampo = "CARTEIRA"
			lTexto = "18-035"
		ENDIF

		IF lcampo = "BANCO_DIG"
			lTexto = "001-9"
		ENDIF

		IF lcampo = "AGENCIA_CODIGO_CEDENTE"
			lTexto = agencia+'-'+d_agencia+'/'+allt(str(val(cc)))+'-'+d_cc
		ENDIF

		IF lcampo = "NOSSO_NUMERO"
			lTexto = nosso_num
		ENDIF

		IF lcampo = "VALORDOC"
			lTexto = alltr(str(valordoc,14,2))
		ENDIF

		IF lcampo = "ESPECIE_DOC"
			lTexto = "RC"
		ENDIF

		IF lcampo = "ESPECIE_MOEDA"
			lTexto = "R$"
		ENDIF

		IF lcampo = "ACEITE"
			lTexto = "N"
		ENDIF

		IF lcampo = "CARTEIRA_NOSSO_NUMERO"
					lTexto = ""
		ENDIF

	CASE LBANCO = "104"
		IF lcampo = "DS_BANCO"
			lTexto = "CAIXA ECONOMICA"
		ENDIF

		IF lcampo = "CARTEIRA"
			lTexto = "SR"
		ENDIF

		IF lcampo = "BANCO_DIG"
			lTexto = "104-0"
		ENDIF

		IF lcampo = "AGENCIA_CODIGO_CEDENTE"
			lTexto = ag_codced
		ENDIF

		IF lcampo = "NOSSO_NUMERO"
			lTexto = nosso_num
		ENDIF

		IF lcampo = "VALORDOC"
			lTexto = valordoc
		ENDIF

		IF lcampo = "ESPECIE_DOC"
			lTexto = "OU"
		ENDIF

		IF lcampo = "ESPECIE_MOEDA"
			lTexto = "R$"
		ENDIF

		IF lcampo = "ACEITE"
			lTexto = "N"
		ENDIF

		IF lcampo = "CARTEIRA_NOSSO_NUMERO"
					lTexto = ""
		ENDIF

	CASE LBANCO = "237"
		IF lcampo = "DS_BANCO"
			lTexto = "BRADESCO"
		ENDIF

		IF lcampo = "CARTEIRA"
			lTexto = "06"
		ENDIF

		IF lcampo = "BANCO_DIG"
			lTexto = "237-2"
		ENDIF

		IF lcampo = "AGENCIA_CODIGO_CEDENTE"
			lTexto = agencia+'-'+d_agencia+'/'+TRANSFORM(cc)+'-'+d_cc
		ENDIF

		IF lcampo = "NOSSO_NUMERO"
			lTexto = "06/"+nosso_num+"-"+dv_ns
		ENDIF

		IF lcampo = "VALORDOC"
			lTexto = alltr(str(valordoc,14,2))
		ENDIF

		IF lcampo = "ESPECIE_DOC"
			lTexto = "DM"
		ENDIF

		IF lcampo = "ESPECIE_MOEDA"
			lTexto = "R$"
		ENDIF

		IF lcampo = "ACEITE"
			lTexto = "N�o"
		ENDIF

		IF lcampo = "CARTEIRA_NOSSO_NUMERO"
					lTexto = ""
		ENDIF

	CASE LBANCO = "341"
		IF lcampo = "DS_BANCO"
			lTexto = "BANCO ITA�"
		ENDIF

		IF lcampo = "CARTEIRA"
			lTexto = SUBSTR(linha1,20,3)
		ENDIF

		IF lcampo = "BANCO_DIG"
			lTexto = "341-7"
		ENDIF

		IF lcampo = "AGENCIA_CODIGO_CEDENTE"
			lTexto = padl(alltr(agencia),4,"0")+'/'+right(cc,5)+'-'+d_cc
		ENDIF

		IF lcampo = "NOSSO_NUMERO"
			DO CASE
				CASE SUBSTR(linha1,20,3) = "198"
					lTexto = right(RIGHT(nossonum,15),7)
				
				CASE SUBSTR(linha1,20,3) = "175"
					lTexto = SUBSTR(linha1,20,3) + "/"+SUBSTR(linha1,23,8) + "-"+SUBSTR(linha1,31,1)
			ENDCASE
		ENDIF

		IF lcampo = "CARTEIRA_NOSSO_NUMERO"
					lTexto = SUBSTR(linha1,20,3) + "/"+ left(RIGHT(ALLTRIM(nossonum),15),8)+ "-"+calc_mod10(AGENCIA+LEFT(CC,5)+SUBSTR(linha1,20,3) +left(RIGHT(ALLTRIM(nossonum),15),8))
		ENDIF

		IF lcampo = "VALORDOC"
			lTexto = alltr(str(valordoc,10,2))
		ENDIF

		IF lcampo = "ESPECIE_DOC"
			lTexto = "DP"
		ENDIF

		IF lcampo = "ESPECIE_MOEDA"
			lTexto = "R$"
		ENDIF

		IF lcampo = "ACEITE"
			lTexto = "N"
		ENDIF

	CASE LBANCO = "033"
		IF lcampo = "DS_BANCO"
			lTexto = "SANTANDER"
		ENDIF

		IF lcampo = "CARTEIRA"
			IF FSIZE("CART_BOL") > 0
				lTexto = cart_bol
			ELSE
				lTexto = "102"
			ENDIF 
		ENDIF

		IF lcampo = "BANCO_DIG"
			lTexto = "033-7"
		ENDIF

		IF lcampo = "AGENCIA_CODIGO_CEDENTE"
			IF FSIZE("AG_CONVEN") > 0 AND FSIZE("CC_CONVEN") > 0
				lTexto = ALLTRIM(AG_CONVEN) + "/" + CC_CONVEN
			ELSE
				lTexto = ALLTRIM(agencia) +[/] + cod_ced
			ENDIF
		ENDIF

		IF lcampo = "NOSSO_NUMERO"
			IF FSIZE("CART_BOL") > 0
				lTexto = alltr(nosso_num)
			ELSE
				lTexto = alltr(nossonum)
			ENDIF
		ENDIF

		IF lcampo = "VALORDOC"
			IF FSIZE("CART_BOL") > 0
				lTexto = ""
			ELSE
				lTexto = valordoc
			ENDIF
		ENDIF

		IF lcampo = "ESPECIE_DOC"
			lTexto = "DM"
		ENDIF

		IF lcampo = "ESPECIE_MOEDA"
			lTexto = "R$"
		ENDIF

		IF lcampo = "ACEITE"
			lTexto = "N"
		ENDIF

		IF lcampo = "CARTEIRA_NOSSO_NUMERO"
			IF FSIZE("CART_BOL") > 0
				lTexto = ALLTRIM(cart_bol)+"/"+STUFF(right(ALLTRIM(nosso_num),13),13,0,[ ])
			ELSE
				lTexto = STUFF(right(nossonum,13),13,0,[ ])
			ENDIF
		ENDIF

	OTHERWISE
		IF lcampo = "INSTRUCOES"
			DO CASE 
				CASE lsigla_assoc = "SBDTFP"
					TEXT TO lTexto NOSHOW PRETEXT 2
					(Pag�vel em qualquer Banco ou Casas Lot�ricas)
					Sr(a). Caixa, TRATA-SE DE DOA��O, n�o cobrar juros ou mora, pode receber valores diferentes.
					ENDTEXT
			ENDCASE
		ENDIF
		
		IF lcampo = "CEDENTE"
			DO CASE 
				CASE lsigla_assoc = "SBDTFP"
					lTexto = "Campanha Vinde Nossa Senhora de F�tima, n�o Tardeis! - (SBDTFP)"

			ENDCASE 
		ENDIF
	
ENDCASE

RETURN lTexto