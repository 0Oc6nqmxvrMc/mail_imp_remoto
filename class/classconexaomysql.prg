Define Class ConexaoBD As Custom
	Protected oConexao          As ADODB.Connection
	Protected oConnHandle       As Integer
	Protected oStringConexao    As String
	Protected oNomeConexao      As String
	Protected oSQLConnect       As Logical
	Protected oDisconect	       As Integer
	Protected oTimeOut          As Integer
	Protected oSecUltimoComando As Integer
	Protected oSecComandoAtual  As Integer
	Protected oMensagem         As String

	Protected Procedure Init As void
		This.oConexao          = Newobject("ADODB.CONNECTION")
		This.oConnHandle       = 0
		This.oTimeOut          = 120
		This.oSecUltimoComando = 0
		This.oSecComandoAtual  = 0
		This.oStringConexao    = ""
		This.oNomeConexao      = ""
		This.oMensagem         = ""
	Endproc

	Procedure ConectaBD As Integer
		Lparameters rNomeConexao As String
		Local lMsg, lException, lErro

		Try
			lMsg = "Iniciando conex�o..."
			lErro = .F.
			This.oNomeConexao = Iif( Vartype(rNomeConexao) # "C", This.oNomeConexao, Alltrim(rNomeConexao) )

			If Empty( This.oNomeConexao )
				lMsg = "O nome da Conex�o n�o foi informado."
				Throw lException
			Endif

			This.TimeOutConexao()

			This.oConnHandle = Iif( Vartype( This.oConnHandle ) # "N", 0, This.oConnHandle )
			If This.oConnHandle <= 0
				This.GetStringConexao()
				If Empty( This.oStringConexao ) And This.oSQLConnect = .F.
					lMsg = "A String de Conex�o n�o foi informada."
					Throw lException
				Endif
				If This.oSQLConnect = .T.
					This.oConnHandle = SQLConnect( This.oNomeConexao )
				Else
					This.oConnHandle = Sqlstringconnect( This.oStringConexao )
				Endif
				If This.oConnHandle < 1
					lMsg = "N�o foi poss�vel estabelecer conex�o com: " + rNomeConexao + "."
					Throw lException
				Endif
			Endif

			lMsg = "Conex�o foi estabelecida com o Banco de Dados"
		Catch To lException
			lErro = .T.
			If lException.ErrorNo # 2071
				lMsg = This.GetException( lException )
			Endif
		Finally
			If lErro = .T.
				This.DesconectaBD()
			Endif
			This.oMensagem = lMsg
		Endtry

		Return This.oConnHandle
	Endproc

	Procedure DesconectaBD As Integer
		Local lMsg, lException

		Try
			lMsg = ""

			This.oConnHandle = Iif( Vartype( This.oConnHandle ) # "N", 0, This.oConnHandle )
			If This.oConnHandle > 0
				This.oDisconect = SQLDisconnect( This.oConnHandle )
				If This.oDisconect < 0
					lMsg = "N�o foi poss�vel encerrar a conex�o."
					Throw lException
				Else
					lMsg = "Conex�o finalizada."
				Endif
			Else
				lMsg = "A conex�o n�o est� ativa para ser encerrada."
				This.oDisconect = 1
			Endif

		Catch To lException
			If lException.ErrorNo # 2071
				lMsg = This.GetException( lException )
			Endif
		Finally
			This.oConnHandle = 0
			This.oMensagem = lMsg
		Endtry

		Return This.oDisconect
	Endproc

	Procedure ExecutaComando As Integer
		Lparameters rComandoSql As String, rCursor As String
		Local lReturn, lMsg, lException, lErro, lCursor

		Try
			lMsg = ""
			lReturn = 0
			lErro = .F.
			lComandoSql = Iif( Vartype( rComandoSql ) = "C", rComandoSql, "")
			lCursor = Iif( Vartype( rCursor ) = "C" And !Empty( rCursor ), rCursor, "SQLRESULT")

			If Vartype( This.oConnHandle ) # "N" Or This.oConnHandle <= 0
				lMsg = "N�o h� conex�o com o Banco de Dados"
				Throw lException
			Endif

			If Used( lCursor ) && Fecha o cursor para evitar utiliza��o da pesquisa anterior
				Select( lCursor )
				Use
			Endif

			lReturn = SQLExec(This.oConnHandle, lComandoSql, lCursor)
			If lReturn > 0
				lMsg = "Comando efetuado com sucesso"
			Else
				lMsg = "Problemas na execu��o do Comando: " + Chr(13) + Chr(10) + rComandoSql + Chr(13) + Chr(10)
				Throw lException
			Endif

		Catch To lException
			lErro = .T.
			If lException.ErrorNo # 2071
				lMsg = This.GetException( lException )
			Endif
		Finally
			If lErro = .T.
				This.DesconectaBD()
			Endif
			This.oMensagem = lMsg
		Endtry

		Return lReturn
	Endproc

	Protected Procedure TimeOutConexao As void
		Local lSegundos

		If This.oTimeOut > 0
			This.oSecComandoAtual = Seconds()
			lSegundos = This.oSecComandoAtual - This.oSecUltimoComando
			If ( lSegundos > This.oTimeOut ) Or lSegundos < 0
				This.oSecUltimoComando = This.oSecComandoAtual
				If This.VerificaConexao() = .F.
					This.DesconectaBD()
					This.ConectaBD( This.oNomeConexao )
				Endif
			Endif
		Endif

	Endproc

	Protected Procedure VerificaConexao As Logical
		Local lReturn, lMsg, lException, lCursor

		Try
			lMsg = "Procedure VerificaConexao."
			lReturn = .F.
			lCursor = "cVerConexao" + Sys(2015)

			* Comando para testar conex�o
			If This.ExecutaComando( "SELECT CURDATE() AS dt_atual", lCursor ) <= 0
				lMsg = This.GetMensagem()
				Throw lException
			Endif

			If Used( lCursor )
				Select ( lCursor )
			Endif

			lMsg = "Verifica��o da conex�o finalizada com sucesso"
			lReturn = .T.
		Catch To lException
			If lException.ErrorNo # 2071
				lMsg = This.GetException( lException )
			Endif
		Finally
			This.oMensagem = lMsg
		Endtry

		Return lReturn
	Endproc

	Protected Procedure GetStringConexao As void
		This.oSQLConnect = .F.
		Do Case
			Case This.oNomeConexao == ""
				This.oStringConexao = "" + ;
					"DRIVER=MySQL ODBC 5.3 ANSI Driver;" + ;
					"SERVER=192.168.24.5;" + ;
					"UID=cep_brasil;" + ;
					"PWD=cep_brasil;" + ;
					"PORT=3306;" + ;
					"DATABASE=dnecom;" + ;
					"WSID="+Substr(Sys(0),1, Atc("#",Sys(0))-1)+";"

			Case !Empty(This.oNomeConexao) && Para o caso de conex�o configurada na fonte de dados ODBC
				This.oSQLConnect = .T.

			Otherwise
				This.oStringConexao = ""
		Endcase
	Endproc

	Procedure GetMensagem As String
		Return This.oMensagem
	Endproc

	Protected Procedure GetException As String
		Parameters rException As Exception
		Local lMsg

		Try

			lMsg = [Mensagem de erro: ] + Chr(13) + Chr(10) + ;
				[Linha: ] + Alltrim(Str(rException.Lineno)) + Chr(13) + Chr(10) + ;
				[Erro: ] + Alltrim(Str(rException.ErrorNo)) + Chr(13) + Chr(10) + ;
				[Mensagem: ] + rException.Message  + Chr(13) + Chr(10) + ;
				[Procedure: ] + rException.Procedure + Chr(13) + Chr(10) + ;
				[Detalhes: ] + rException.Procedure + Chr(13) + Chr(10) + ;
				[StackLevel: ] + Alltrim(Str(rException.StackLevel)) + Chr(13) + Chr(10) + ;
				[Conte�do Linha: ] + rException.LineContents + Chr(13) + Chr(10)

		Catch To lException
			lMsg = "Erro n�o identificado ao executar a Classe ConexaoBD."
		Finally
			This.oMensagem = lMsg
		Endtry

		Return lMsg
	Endproc

	Protected Procedure Destroy As void
		This.DesconectaBD()
		This.oConexao = Null
		This.Destroy()
	Endproc

Enddefine
